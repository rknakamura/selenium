﻿namespace Selenium
{
    public class Connector
    {
        public PageSetting And => new PageSetting();
        public PageExpectation Then => new PageExpectation();
    }
}
