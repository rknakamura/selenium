﻿using OpenQA.Selenium;
using System.Threading;

namespace Selenium
{
    public class Action
    {
        private IWebElement _element;
        private Connector _connector;

        public Action(IWebElement element)
        {
            _element = element;
            _connector = new Connector();
        }

        public Connector Click()
        {
            _element.Click();
            return _connector;
        }        

        public Connector Click(int timeout)
        {
            Thread.Sleep(timeout);
            return Click();
        }

        public Connector Clear()
        {
            _element.Clear();
            return _connector;
        }

        public Connector Clear(int timeout)
        {
            Thread.Sleep(timeout);
            return Clear();
        }

        public Connector SendKeys(string value)
        {
            _element.SendKeys(value);
            return _connector;
        }

        public Connector SendKeys(string value, int timeout)
        {
            Thread.Sleep(timeout);
            return SendKeys(value);
        }        
    }
}
