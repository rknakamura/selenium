﻿using System;

namespace Selenium
{
    public class Given
    {
        public static Func<string, PageSetting> Website = (url) =>
        {
            WebDriver.Driver.Url = url;
            return new PageSetting();
        };
    }
}
