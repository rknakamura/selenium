﻿using SeleniumExtras.PageObjects;

namespace Selenium
{
    public class Page<T> where T : class, new()
    {
        public static T Load()
        {
            var page = new T();
            PageFactory.InitElements(WebDriver.Driver, page);

            return page;
        }
    }
}
