﻿using OpenQA.Selenium;

namespace Selenium
{
    public class PageExpectation
    {
        public Action Then(IWebElement element)
            => new Action(element);
    }
}
