﻿using OpenQA.Selenium;

namespace Selenium
{
    public class PageSetting
    {
        public Action When(IWebElement element)
            => new Action(element);
    }
}
