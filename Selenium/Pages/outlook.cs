using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;

public class outlook
{
    public Dictionary<string, string> data;
    public IWebDriver driver;
    public int timeout = 15;

    [FindsBy(How = How.CssSelector, Using = "a.buyO365Button")]
    [CacheLookup]
    public IWebElement comprarOOffice3651;

    [FindsBy(How = How.CssSelector, Using = "section.landing div div.app div:nth-of-type(3) div.landing-sectionCopy div.landing-sectionImgs div.section-subheader.sectionFive-subheader a.linkButtonSigninHeader.linkButtonSigninHeader--premium")]
    [CacheLookup]
    public IWebElement comprarOOffice3652;

    [FindsBy(How = How.CssSelector, Using = "section.landing div div.app header.fixedHeader div.fixedHeaderNav.clearfix div.headerContainer div.fixedHeaderMenu nav.linkButtonFixedHeader-mobileMenu a:nth-of-type(1)")]
    [CacheLookup]
    public IWebElement criarContaGratuita1;

    [FindsBy(How = How.CssSelector, Using = "section.landing div div.app div:nth-of-type(2) a:nth-of-type(1)")]
    [CacheLookup]
    public IWebElement criarContaGratuita2;

    [FindsBy(How = How.CssSelector, Using = "section.landing div div.app div:nth-of-type(5) div.footerNav.clearfix div:nth-of-type(2) a:nth-of-type(2)")]
    [CacheLookup]
    public IWebElement criarContaGratuita3;

    [FindsBy(How = How.CssSelector, Using = "a.linkButtonFixedHeader.office-signIn")]
    [CacheLookup]
    public IWebElement entrar1;

    [FindsBy(How = How.CssSelector, Using = "a[href='#']")]
    [CacheLookup]
    public IWebElement entrar2;

    [FindsBy(How = How.CssSelector, Using = "a[href='http://support.microsoft.com/contactus/']")]
    [CacheLookup]
    public IWebElement faleConosco;

    [FindsBy(How = How.CssSelector, Using = "a[href='http://www.microsoft.com/en-us/legal/intellectualproperty/copyright/default.aspx']")]
    [CacheLookup]
    public IWebElement legal;

    [FindsBy(How = How.CssSelector, Using = "a.outlookAppstore")]
    [CacheLookup]
    public IWebElement linkDaAppStore;

    [FindsBy(How = How.CssSelector, Using = "a.outlookWinstore")]
    [CacheLookup]
    public IWebElement linkDaWindowsStore;

    [FindsBy(How = How.CssSelector, Using = "a.outlookPlaystore")]
    [CacheLookup]
    public IWebElement linkDoGooglePlay;

    [FindsBy(How = How.CssSelector, Using = "a[href='http://www.microsoft.com/trademarks']")]
    [CacheLookup]
    public IWebElement marcas;

    [FindsBy(How = How.CssSelector, Using = "section.landing div div.app nav.officeHeader div.officeHeaderNav.clearfix div.headerContainer a")]
    [CacheLookup]
    public IWebElement microsoft;

    [FindsBy(How = How.CssSelector, Using = "section.landing div div.app div:nth-of-type(5) div.footerNav.clearfix div:nth-of-type(1) a")]
    [CacheLookup]
    public IWebElement microsoftLogo;

    [FindsBy(How = How.CssSelector, Using = "a[href='https://outlook.com']")]
    [CacheLookup]
    public IWebElement outlook1;

    public readonly string pageLoadedText = "O Outlook reúne todos os seus emails, eventos de calendário e arquivos para que você não tenha o trabalho de fazer";

    public readonly string pageUrl = "/owa/";

    [FindsBy(How = How.CssSelector, Using = "a.linkButtonFixedFooter.privacy")]
    [CacheLookup]
    public IWebElement privacidadeECookies;

    [FindsBy(How = How.CssSelector, Using = "section.landing div div.app header.fixedHeader div.fixedHeaderNav.clearfix div.headerContainer div.fixedHeaderMenu nav.linkButtonFixedHeader-mobileMenu a:nth-of-type(2)")]
    [CacheLookup]
    public IWebElement suporte1;

    [FindsBy(How = How.CssSelector, Using = "section.landing div div.app div:nth-of-type(5) div.footerNav.clearfix div:nth-of-type(2) a:nth-of-type(1)")]
    [CacheLookup]
    public IWebElement suporte2;

    public outlook()
        : this(default(IWebDriver), new Dictionary<string, string>(), 15)
    {
    }

    public outlook(IWebDriver driver)
        : this(driver, new Dictionary<string, string>(), 15)
    {
    }

    public outlook(IWebDriver driver, Dictionary<string, string> data)
        : this(driver, data, 15)
    {
    }

    public outlook(IWebDriver driver, Dictionary<string, string> data, int timeout)
    {
        this.driver = driver;
        this.data = data;
        this.timeout = timeout;
    }

    /// <summary>
    /// Click on Comprar O Office 365 Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickComprarOOffice3651Link() 
    {
        comprarOOffice3651.Click();
        return this;
    }

    /// <summary>
    /// Click on Comprar O Office 365 Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickComprarOOffice3652Link() 
    {
        comprarOOffice3652.Click();
        return this;
    }

    /// <summary>
    /// Click on Criar Conta Gratuita Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickCriarContaGratuita1Link() 
    {
        criarContaGratuita1.Click();
        return this;
    }

    /// <summary>
    /// Click on Criar Conta Gratuita Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickCriarContaGratuita2Link() 
    {
        criarContaGratuita2.Click();
        return this;
    }

    /// <summary>
    /// Click on Criar Conta Gratuita Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickCriarContaGratuita3Link() 
    {
        criarContaGratuita3.Click();
        return this;
    }

    /// <summary>
    /// Click on Entrar Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickEntrar1Link() 
    {
        entrar1.Click();
        return this;
    }

    /// <summary>
    /// Click on Entrar Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickEntrar2Link() 
    {
        entrar2.Click();
        return this;
    }

    /// <summary>
    /// Click on Fale Conosco Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickFaleConoscoLink() 
    {
        faleConosco.Click();
        return this;
    }

    /// <summary>
    /// Click on Legal Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickLegalLink() 
    {
        legal.Click();
        return this;
    }

    /// <summary>
    /// Click on Link Da App Store Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickLinkDaAppStoreLink() 
    {
        linkDaAppStore.Click();
        return this;
    }

    /// <summary>
    /// Click on Link Da Windows Store Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickLinkDaWindowsStoreLink() 
    {
        linkDaWindowsStore.Click();
        return this;
    }

    /// <summary>
    /// Click on Link Do Google Play Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickLinkDoGooglePlayLink() 
    {
        linkDoGooglePlay.Click();
        return this;
    }

    /// <summary>
    /// Click on Marcas Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickMarcasLink() 
    {
        marcas.Click();
        return this;
    }

    /// <summary>
    /// Click on Microsoft Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickMicrosoftLink() 
    {
        microsoft.Click();
        return this;
    }

    /// <summary>
    /// Click on Microsoft Logo Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickMicrosoftLogoLink() 
    {
        microsoftLogo.Click();
        return this;
    }

    /// <summary>
    /// Click on Outlook Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickOutlookLink() 
    {
        outlook1.Click();
        return this;
    }

    /// <summary>
    /// Click on Privacidade E Cookies Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickPrivacidadeECookiesLink() 
    {
        privacidadeECookies.Click();
        return this;
    }

    /// <summary>
    /// Click on Suporte Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickSuporte1Link() 
    {
        suporte1.Click();
        return this;
    }

    /// <summary>
    /// Click on Suporte Link.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook ClickSuporte2Link() 
    {
        suporte2.Click();
        return this;
    }

    /// <summary>
    /// Verify that the page loaded completely.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook VerifyPageLoaded() 
    {
        new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until<bool>((d) =>
        {
            return d.PageSource.Contains(pageLoadedText);
        });
        return this;
    }

    /// <summary>
    /// Verify that current page URL matches the expected URL.
    /// </summary>
    /// <returns>The outlook class instance.</returns>
    public outlook VerifyPageUrl() 
    {
        new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until<bool>((d) =>
        {
            return d.Url.Contains(pageUrl);
        });
        return this;
    }
}
