using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

public class outlook1
{
    public Dictionary<string, string> data;
    public IWebDriver driver;
    public int timeout = 15;

    [FindsBy(How = How.Id, Using = "signup")]
    [CacheLookup]
    public IWebElement crieUma;

    [FindsBy(How = How.Id, Using = "i0116")]
    [CacheLookup]
    public IWebElement noTemUmaConta1;

    [FindsBy(How = How.Id, Using = "i0118")]
    [CacheLookup]
    public IWebElement noTemUmaConta2;

    public readonly string pageLoadedText = "Não tem uma conta";

    public readonly string pageUrl = "/login.srf?wa=wsignin1.0&rpsnv=13&ct=1540862020&rver=7.0.6737.0&wp=MBI_SSL&wreply=https%3a%2f%2foutlook.live.com%2fowa%2f%3fnlp%3d1%26RpsCsrfState%3db8b918a5-0631-ec34-b729-70ec349370ce&id=292841&CBCXT=out&lw=1&fl=dob%2cflname%2cwld&cobrandid=90015";

    [FindsBy(How = How.Id, Using = "ftrPrivacy")]
    [CacheLookup]
    public IWebElement privacidadeECookies;

    [FindsBy(How = How.Id, Using = "idSIButton9")]
    [CacheLookup]
    public IWebElement prximo;

    [FindsBy(How = How.Id, Using = "ftrTerms")]
    [CacheLookup]
    public IWebElement termosDeUso;

    public outlook1()
        : this(default(IWebDriver), new Dictionary<string, string>(), 15)
    {
    }

    public outlook1(IWebDriver driver)
        : this(driver, new Dictionary<string, string>(), 15)
    {
    }

    public outlook1(IWebDriver driver, Dictionary<string, string> data)
        : this(driver, data, 15)
    {
    }

    public outlook1(IWebDriver driver, Dictionary<string, string> data, int timeout)
    {
        this.driver = driver;
        this.data = data;
        this.timeout = timeout;
    }

    /// <summary>
    /// Click on Crie Uma Link.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 ClickCrieUmaLink()
    {
        crieUma.Click();
        return this;
    }

    /// <summary>
    /// Click on Privacidade E Cookies Link.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 ClickPrivacidadeECookiesLink()
    {
        privacidadeECookies.Click();
        return this;
    }

    /// <summary>
    /// Click on Prximo Button.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 ClickPrximoButton()
    {
        prximo.Click();
        return this;
    }

    /// <summary>
    /// Click on Termos De Uso Link.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 ClickTermosDeUsoLink()
    {
        termosDeUso.Click();
        return this;
    }

    /// <summary>
    /// Fill every fields in the page.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 Fill()
    {
        SetNoTemUmaConta1PasswordField();
        SetNoTemUmaConta2PasswordField();
        return this;
    }

    /// <summary>
    /// Fill every fields in the page and submit it to target page.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 FillAndSubmit()
    {
        Fill();
        return Submit();
    }

    /// <summary>
    /// Set default value to No Tem Uma Conta Password field.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 SetNoTemUmaConta1PasswordField()
    {
        return SetNoTemUmaConta1PasswordField(data["NO_TEM_UMA_CONTA_1"]);
    }

    /// <summary>
    /// Set value to No Tem Uma Conta Password field.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 SetNoTemUmaConta1PasswordField(string noTemUmaConta1Value)
    {
        noTemUmaConta1.SendKeys(noTemUmaConta1Value);
        return this;
    }

    /// <summary>
    /// Set default value to No Tem Uma Conta Password field.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 SetNoTemUmaConta2PasswordField()
    {
        return SetNoTemUmaConta2PasswordField(data["NO_TEM_UMA_CONTA_2"]);
    }

    /// <summary>
    /// Set value to No Tem Uma Conta Password field.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 SetNoTemUmaConta2PasswordField(string noTemUmaConta2Value)
    {
        noTemUmaConta2.SendKeys(noTemUmaConta2Value);
        return this;
    }

    /// <summary>
    /// Submit the form to target page.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 Submit()
    {
        ClickPrximoButton();
        return this;
    }

    /// <summary>
    /// Verify that the page loaded completely.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 VerifyPageLoaded()
    {
        new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until<bool>((d) =>
        {
            return d.PageSource.Contains(pageLoadedText);
        });
        return this;
    }

    /// <summary>
    /// Verify that current page URL matches the expected URL.
    /// </summary>
    /// <returns>The outlook1 class instance.</returns>
    public outlook1 VerifyPageUrl()
    {
        new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until<bool>((d) =>
        {
            return d.Url.Contains(pageUrl);
        });
        return this;
    }
}
