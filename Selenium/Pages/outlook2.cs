using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

public class outlook2
{
    public Dictionary<string, string> data;
    public IWebDriver driver;
    public int timeout = 15;

    [FindsBy(How = How.CssSelector, Using = ".btn.btn-block.btn-primary")]
    [CacheLookup]
    public IWebElement entrar;

    [FindsBy(How = How.Id, Using = "idA_PWD_ForgotPassword")]
    [CacheLookup]
    public IWebElement esqueciMinhaSenha;

    [FindsBy(How = How.Name, Using = "loginfmt")]
    [CacheLookup]
    public IWebElement mantenhameConectado1;

    [FindsBy(How = How.Id, Using = "i0118")]
    [CacheLookup]
    public IWebElement mantenhameConectado2;

    [FindsBy(How = How.Id, Using = "idChkBx_PWD_KMSI0Pwd")]
    [CacheLookup]
    public IWebElement mantenhameConectado3;

    public readonly string pageLoadedText = "";

    public readonly string pageUrl = "/login.srf?wa=wsignin1.0&rpsnv=13&ct=1540862020&rver=7.0.6737.0&wp=MBI_SSL&wreply=https%3a%2f%2foutlook.live.com%2fowa%2f%3fnlp%3d1%26RpsCsrfState%3db8b918a5-0631-ec34-b729-70ec349370ce&id=292841&CBCXT=out&lw=1&fl=dob%2cflname%2cwld&cobrandid=90015";

    [FindsBy(How = How.Id, Using = "ftrPrivacy")]
    [CacheLookup]
    public IWebElement privacidadeECookies;

    [FindsBy(How = How.Id, Using = "ftrTerms")]
    [CacheLookup]
    public IWebElement termosDeUso;

    public outlook2()
        : this(default(IWebDriver), new Dictionary<string, string>(), 15)
    {
    }

    public outlook2(IWebDriver driver)
        : this(driver, new Dictionary<string, string>(), 15)
    {
    }

    public outlook2(IWebDriver driver, Dictionary<string, string> data)
        : this(driver, data, 15)
    {
    }

    public outlook2(IWebDriver driver, Dictionary<string, string> data, int timeout)
    {
        this.driver = driver;
        this.data = data;
        this.timeout = timeout;
    }

    /// <summary>
    /// Click on Entrar Button.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 ClickEntrarButton() 
    {
        entrar.Click();
        return this;
    }

    /// <summary>
    /// Click on Esqueci Minha Senha Link.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 ClickEsqueciMinhaSenhaLink() 
    {
        esqueciMinhaSenha.Click();
        return this;
    }

    /// <summary>
    /// Click on Privacidade E Cookies Link.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 ClickPrivacidadeECookiesLink() 
    {
        privacidadeECookies.Click();
        return this;
    }

    /// <summary>
    /// Click on Termos De Uso Link.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 ClickTermosDeUsoLink() 
    {
        termosDeUso.Click();
        return this;
    }

    /// <summary>
    /// Fill every fields in the page.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 Fill() 
    {
        SetMantenhameConectado1PasswordField();
        SetMantenhameConectado2PasswordField();
        SetMantenhameConectado3CheckboxField();
        return this;
    }

    /// <summary>
    /// Fill every fields in the page and submit it to target page.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 FillAndSubmit() 
    {
        Fill();
        return Submit();
    }

    /// <summary>
    /// Set default value to Mantenhame Conectado Password field.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 SetMantenhameConectado1PasswordField() 
    {
        return SetMantenhameConectado1PasswordField(data["MANTENHAME_CONECTADO_1"]);
    }

    /// <summary>
    /// Set value to Mantenhame Conectado Password field.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 SetMantenhameConectado1PasswordField(string mantenhameConectado1Value)
    {
        mantenhameConectado1.SendKeys(mantenhameConectado1Value);
        return this;
    }

    /// <summary>
    /// Set default value to Mantenhame Conectado Password field.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 SetMantenhameConectado2PasswordField() 
    {
        return SetMantenhameConectado2PasswordField(data["MANTENHAME_CONECTADO_2"]);
    }

    /// <summary>
    /// Set value to Mantenhame Conectado Password field.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 SetMantenhameConectado2PasswordField(string mantenhameConectado2Value)
    {
        mantenhameConectado2.SendKeys(mantenhameConectado2Value);
        return this;
    }

    /// <summary>
    /// Set Mantenhame Conectado Checkbox field.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 SetMantenhameConectado3CheckboxField() 
    {
        if (!mantenhameConectado3.Selected) {
            mantenhameConectado3.Click();
        }
        return this;
    }

    /// <summary>
    /// Submit the form to target page.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 Submit() 
    {
        ClickEntrarButton();
        return this;
    }

    /// <summary>
    /// Unset Mantenhame Conectado Checkbox field.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 UnsetMantenhameConectado3CheckboxField() 
    {
        if (mantenhameConectado3.Selected) {
            mantenhameConectado3.Click();
        }
        return this;
    }

    /// <summary>
    /// Verify that the page loaded completely.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 VerifyPageLoaded() 
    {
        new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until<bool>((d) =>
        {
            return d.PageSource.Contains(pageLoadedText);
        });
        return this;
    }

    /// <summary>
    /// Verify that current page URL matches the expected URL.
    /// </summary>
    /// <returns>The outlook2 class instance.</returns>
    public outlook2 VerifyPageUrl() 
    {
        new WebDriverWait(driver, TimeSpan.FromSeconds(timeout)).Until<bool>((d) =>
        {
            return d.Url.Contains(pageUrl);
        });
        return this;
    }
}
