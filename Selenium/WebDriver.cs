﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Selenium
{
    public static class WebDriver
    {
        private const string pathDriver = @"C:\Users\Win8\source\repos\Selenium\Selenium\bin\Debug\netcoreapp2.1";
        private static IWebDriver _driver;

        public static IWebDriver Driver => GetDriver();

        private static IWebDriver GetDriver()
        {
            if (_driver == null)
                _driver = new ChromeDriver(pathDriver);

            return _driver;
        }

    }
}
